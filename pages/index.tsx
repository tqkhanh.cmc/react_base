import 'react-toastify/dist/ReactToastify.css';

import { Container } from '@components/layout';
import { TodoList } from '@views/home';
import React, { Suspense, useEffect, useState } from 'react';
import { API } from 'src/lib';
import axios from 'axios';
import Input from '@components/layout/input/Input';
import Card from '@components/layout/card/Card';
import Button from '@components/layout/button/Button';

/**
 * Home component
 * @returns
 */

export const QUESTION_URL = 'https://jservice.io/api/random'

const Home = () => {
  const [question, setQuestion] = useState("")
  const [answer, setAnswer] = useState("")
  const [userAnswer, setUserAnswer] = useState("")
  const [result, setResult] = useState(false)
  const [showResultText, setShowResultText] = useState(false)

  const getQuestion = () => {
    axios.get(QUESTION_URL).then(response => response.data)
      .catch(function (error) {
        // handle error
        console.log(error);
      })
      .then(function (data: Random.Res[]) {
        // always executed
        data && setQuestion(data[0].question)
        setAnswer(data[0].answer)
        setShowResultText(false)
      });
  }

  const onSetUserAnswers = (event) => {
    setUserAnswer(event.target.value)
  }

  const onSubmit = () => {
    if (userAnswer) {
      if (userAnswer.trim() === answer.trim()) {
        setResult(true)
        setUserAnswer("")
        getQuestion()
      } else {
        setResult(false)
      }
      setShowResultText(true)
    } else {
      window.alert('You must enter your answer first!')
    }

  }

  useEffect(() => {
    getQuestion()
  }, [])

  return (
    < Container >
      {showResultText && <p style={{ fontSize: 'large', textAlign: 'center' }}>{result ? 'Correct!' : 'Incorrect!'}</p>}
      {question ? <Card
        header={question}
        body={<div style={{ alignItems: 'center', display: 'flex', justifyContent: 'space-between' }}>
          <Input value={userAnswer} onChange={onSetUserAnswers} placeholder={"Your answer"} />
          <Button title={'Submit'} onClick={onSubmit} />
        </div>}
      /> :
        <p>Loading question...</p>
      }
    </ Container>
  );
}

export default Home;
