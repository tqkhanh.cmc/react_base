import React from 'react'
import { render, fireEvent } from "@testing-library/react";
import Input from '../components/layout/input/Input'
import { InputProps } from '@components/layout/input/Input.props';

const renderInput = (props: Partial<InputProps> = {}) => {
  return render(<Input {...props} />)
}

describe("<Input/>", () => {
  test("Should display input with blank value", () => {
    renderInput()

  })
})