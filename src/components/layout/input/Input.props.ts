export type InputProps = {
  value?: any,
  onChange?: (e) => void,
  placeholder?: string,
}