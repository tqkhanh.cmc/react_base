import React from "react";
import { InputProps } from "./Input.props";
import styles from './Input.module.scss'

const Input = (props: InputProps) => {
  return (
    <input
      className={styles.input}
      value={props.value}
      onChange={props.onChange}
      placeholder={props.placeholder}
    />
  )
}

export default Input