import React from "react";
import { ButtonProps } from "./Button.props";
import styles from './Button.module.scss'

const Button = (props: ButtonProps) => {
  return (
    <button
      className={props.mode === 'primary' ? styles.primary_btn : styles.primary_btn}
      onClick={props.onClick}

    >
      {props.title}
    </button>
  )
}

export default Button