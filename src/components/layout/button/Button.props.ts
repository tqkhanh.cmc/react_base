export type ButtonProps = {
  title: string,
  onClick: ()=>void,
  mode?: string
}