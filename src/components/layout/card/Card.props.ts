import React from "react";

export type CardProps = {
  header: React.ReactNode | string,
  children?: React.ReactNode,
  body?:React.ReactNode
}