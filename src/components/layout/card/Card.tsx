import React from "react";
import { CardProps } from "./Card.props";
import styles from './Card.module.scss';

const Card = (props: CardProps) => {
  return (
    <div className={styles.card}>
      <div className={styles.header_container}>
        <p className={styles.header}>{props.header}</p>
      </div>
      {props.body||<div className={styles.body}></div>}
      {props.children}
    </div>
  )
}

export default Card